#!/usr/bin/env python3

import os
import shutil

labelDir = "/media/linux/STORE/lpr-project/work/detected-img/"
imageDir = "/media/linux/STORE/lpr-project/work/detected-img/reviewed-annotated/"
print(os.listdir(os.getcwd()))

for filename in os.listdir(labelDir):
	filepath = labelDir + filename
	filename = filename.replace(".txt",".jpg",1)
	if filename in os.listdir(imageDir):
		shutil.move(filepath, imageDir+filepath)
