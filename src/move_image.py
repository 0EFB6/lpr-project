#!/usr/bin/env python3

import os
import shutil

fileDir = "/home/linux/note.txt"
srcDir = "/media/linux/STORE/lpr-project/work/detected-img/labels/"
destDir = "/media/linux/STORE/lpr-project/work/detected-img/output-2/"

with open(fileDir, 'r') as file:
	filedata = file.readlines()
	for line in filedata:
		line = line.strip()
		if line in os.listdir(srcDir):
			shutil.move(srcDir+line, destDir+line)
file.close()
