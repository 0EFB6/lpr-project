#!/usr/bin/env python3
import os

for filename in os.listdir(os.getcwd()):
	with open(os.path.join(os.getcwd(), filename), 'r') as file:
		filedata = file.readlines()
	for line in filedata:
		line = line.replace('0', 'license-plate', 1)
		with open(os.path.join(os.getcwd(),"new-"+filename), 'a') as file2:
			file2.write(line)
	file.close()
	file2.close()
