#!/usr/bin/env python3

import os
import shutil

srcDir = "/media/linux/STORE/lpr-project/work/detected-img/ori-img/"
destDir = "/media/linux/STORE/lpr-project/work/detected-img/"
refDir = "/media/linux/STORE/lpr-project/work/detected-img/reviewed-annotated/"
print(os.listdir(os.getcwd()))

for file in os.listdir(refDir):
	if file in os.listdir(srcDir):
		shutil.move(srcDir+file, destDir+file)
